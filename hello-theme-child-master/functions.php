<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );


function child_init() {
	add_post_type_support( 'page', 'excerpt' );
	// add_post_type_support( 'location', 'excerpt' );
	// add_post_type_support( 'provider', 'excerpt' );
	register_taxonomy_for_object_type( 'category', 'page' );
}
add_action('init', 'child_init');


// Dynamic Content sets this if installed and configured
// function my_acf_init() {
//     acf_update_setting('google_api_key', 'AIzaSyDiCS_dACA0fceubIfJ0uAO-7abo9H02K0');
// }
// add_action('acf/init', 'my_acf_init');

function ele_disable_page_title( $return ) {
	return false;
}
add_filter( 'hello_elementor_page_title', 'ele_disable_page_title');

/**
 * Change navigation submenu to open on click
 */
function elementor_pro_dequeue_script() {
    wp_dequeue_script('smartmenus');
}
add_action('wp_footer', 'elementor_pro_dequeue_script', 15);

function custom_smartmenu_js() {
    wp_enqueue_script('override-menu', get_stylesheet_directory_uri() . '/js/jquery.smartmenus.min.js', array('jquery'), '', true);
}

add_action('wp_enqueue_scripts', 'custom_smartmenu_js');

// // Change default image for custom post type
// uses 'Default featured image' plugin  https://wordpress.org/plugins/default-featured-image/
// function dfi_posttype_book ( $dfi_id, $post_id ) {
//   $return = $dfi_id;
//   $post = get_post($post_id);
//   if ( 'custom-post-slug' === $post->post_type ) {
// 	  $return = 998; // custom-image.png (replace ID from media library)
	  
// 	  $custom_field_selector_key = get_field( "custom_field_selector_key" );
// 	  if ($custom_field_selector_key==0) {
// 		  $return = 999; // custom-image.png (replace ID from media library)
// 	  }	  
    
//   }
//   return $return; // the original featured image id
// }
// add_filter( 'dfi_thumbnail_id', 'dfi_posttype_book', 10, 2 );